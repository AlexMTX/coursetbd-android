package com.example.intencitycounter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class WatchActivity extends Activity {
	
	TextView txtView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watch);

		Intent intent = getIntent();
		
		txtView = (TextView) findViewById(R.id.txt_watch);
		
		String text = null;
		String text2 = "";
		FileInputStream input = null;
		InputStreamReader inputStreamReader = null;
		
		try {
			input = openFileInput("measurement.xml");
			System.out.println(input);
			inputStreamReader = new InputStreamReader(input);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			StringBuilder sb = new StringBuilder();
			while ((text = bufferedReader.readLine()) != null) {
		        sb.append(text);
		        System.out.println(text);
		        if (text != null) text2 = text2 + "\n" + text;
		    }
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		txtView.setText(text2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.watch, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
