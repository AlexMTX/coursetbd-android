package com.example.intencitycounter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class SettingActivity extends Activity implements OnItemSelectedListener  {
	
	EditText streetName1, streetName2;
	TextView latitude, longitude;
	public final static String EXTRA_MESSAGE = "com.example.intetcitycounter.MESSAGE_FROM_SETTING";
	
	Spinner spinner;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		Intent intent = getIntent();
		
		streetName1 = (EditText) findViewById(R.id.set_street_name_1);
		streetName2 = (EditText) findViewById(R.id.set_street_name_2);
		
		latitude = (TextView) findViewById(R.id.coordinates_latitude);
		longitude = (TextView) findViewById(R.id.coordinates_longitude);
		
		spinner = (Spinner) findViewById(R.id.spinner_outpoints);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.outpoints, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
	}
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		spinner.setSelection(position);
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(position)
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void onNext(View view) {
    	Intent intent = new Intent(this, MeasurementActivity.class);
    	ArrayList<String> message = new ArrayList<String>();
    	message.add(streetName1.getText().toString());	// 0: 1-�� ����� 
    	message.add(streetName2.getText().toString());	// 1: 2-�� �����
    	message.add(latitude.getText().toString());	// 2: latitude
    	message.add(longitude.getText().toString());	// 3: longitude
    	message.add(spinner.getSelectedItem().toString());	// 4: ���-�� �������
    	message.add("�������"); // 5: ���
    	intent.putExtra(EXTRA_MESSAGE, message);
    	startActivity(intent);
    }
    
    public void getCoordinates(View view) {
    	LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    	latitude.setText(Double.toString(mlocManager.
    						getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude()));
    	longitude.setText(Double.toString(mlocManager.
    						getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude()));
    }
}
