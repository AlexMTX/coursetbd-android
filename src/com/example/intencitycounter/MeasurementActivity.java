package com.example.intencitycounter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.example.intencitycounter.simpleClasses.Destination;
import com.example.intencitycounter.simpleClasses.DirectionSC;
import com.example.intencitycounter.simpleClasses.Form;
import com.example.intencitycounter.simpleClasses.Intersection;
import com.example.intencitycounter.simpleClasses.LocationSC;
import com.example.intencitycounter.simpleClasses.MeasurementList;
import com.example.intencitycounter.simpleClasses.OriginSC;

public class MeasurementActivity extends Activity {

	private boolean isMeasurementDone;	
	private int carsCount = 0, mediumCount = 0, heavyCount = 0, truckCount = 0, busesCount = 0;
	private TextView txtCars, txtMedium, txtHeavy, txtTruck, txtBuses;
	private ArrayList<String> message;
	private String startTime, startDate;
	private int period = 15;
	
	final int DIALOG_EXIT = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_measurement);
		
		Intent intent = getIntent();
		message = intent.getStringArrayListExtra(SettingActivity.EXTRA_MESSAGE);

		final Chronometer mChronometer = (Chronometer) findViewById(R.id.chronometer_measurement);
		
		txtCars = (TextView) findViewById(R.id.cars);
		txtMedium = (TextView) findViewById(R.id.medium_cargo);
		txtHeavy = (TextView) findViewById(R.id.heavy_cargo);
		txtTruck = (TextView) findViewById(R.id.truck);
		txtBuses = (TextView) findViewById(R.id.buses);
		
		txtCars.setText(Integer.toString(carsCount));
		txtMedium.setText(Integer.toString(mediumCount));
		txtHeavy.setText(Integer.toString(heavyCount));
		txtTruck.setText(Integer.toString(truckCount));
		txtBuses.setText(Integer.toString(busesCount));
		
		//Button button = (Button) findViewById(R.id.button_add_cars);
		
		
		mChronometer.start();
		
		startTime = (String) DateFormat.format("kk:mm:ss",new Date());
		startDate = (String) DateFormat.format("yyyy-MM-dd",new Date());
		isMeasurementDone = false;
		
		mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {

			@Override
			public void onChronometerTick(Chronometer chronometer) {
				long elapsedMillis = SystemClock.elapsedRealtime()
						- mChronometer.getBase();

				if (elapsedMillis > 10000 && elapsedMillis < 11000) {	//840000
					String strElapsedTime = "�������� 1 ������";
					Toast.makeText(MeasurementActivity.this, strElapsedTime, Toast.LENGTH_SHORT).show();
				}
				
				if (elapsedMillis > 20000 && elapsedMillis < 21000) {	//900000
					String strElapsedTime = "15 ����� ������";
					isMeasurementDone = true;
					mChronometer.stop();
					Toast.makeText(MeasurementActivity.this, strElapsedTime, Toast.LENGTH_SHORT).show();
					// ����� �������
					//showDialog(1, savedInstanceState);
				}
			}	
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {	
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.measurement, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void addCars(View view) {
		carsCount++;
		txtCars.setText(Integer.toString(carsCount));
	}
	public void addMedium(View view) {
		mediumCount++;
		txtMedium.setText(Integer.toString(mediumCount));
	}
	public void addHeavy(View view) {
		heavyCount++;
		txtHeavy.setText(Integer.toString(heavyCount));
	}
	public void addTruck(View view) {
		truckCount++;
		txtTruck.setText(Integer.toString(truckCount));
	}
	public void addBuses(View view) {
		busesCount++;
		txtBuses.setText(Integer.toString(busesCount));
	}
	
	public void saveMeasurements(View view) throws Exception {
		Serializer serializer = new Persister();
		
		Collection<MeasurementList> oneDirectionMeasurements = new ArrayList<MeasurementList>();
		String[] transportNames = {"��������", "������� ��������", 
										"������� ��������", "����������", "��������"};
		double[] modes = {1.0, 2.5, 3, 4, 3};
		int[] transportMeasurements = {carsCount, mediumCount, heavyCount, truckCount, busesCount};
		
		for (int i = 0; i < 5; i++) {
			oneDirectionMeasurements.add(new MeasurementList(transportNames[i], modes[i], 
											transportMeasurements[i]));
		}
		
		DirectionSC direction = new DirectionSC(2, message.get(1));
		
		Destination destination = new Destination(direction, oneDirectionMeasurements);
		
		Collection<Destination> destinationList = new ArrayList<Destination>();
		destinationList.add(destination);
		
		Collection<String> col = new ArrayList<String>();
		col.add(message.get(0));	//����� 1
		col.add(message.get(1));	//����� 2 
		
		Intersection intersection = new Intersection(Double.parseDouble(message.get(2)), 
														Double.parseDouble(message.get(3)), col);
		LocationSC location = new LocationSC("�����-���������", "������");
		OriginSC origin = new OriginSC(1, message.get(0));
		
		Form form = new Form(startDate, message.get(5), startTime, getPeriod(), 
								intersection, location, origin, destinationList);
		File result = new File(getFilesDir() + "/measurement.xml");

		serializer.write(form, result);
		Toast.makeText(MeasurementActivity.this, "measurement.xml ��������", Toast.LENGTH_SHORT).show();
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	    
	public int getCarsCount() {
		return carsCount;
	}

	public void setCarsCount() {
		this.carsCount++;
	}

	public int getHeavyCount() {
		return heavyCount;
	}

	public void setHeavyCount() {
		this.heavyCount++;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/*@SuppressWarnings("deprecation")
	private void callDialog() {
		showDialog(DIALOG_EXIT, );
	}*/
	
	/*protected Dialog onCreateDialog(int id) {
		if (id == DIALOG_EXIT) {
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			// ���������
			adb.setTitle(R.string.elapsed15min);
			// ���������
	        adb.setMessage(R.string.whatToDo);
	        // ������
	        adb.setIcon(android.R.drawable.ic_dialog_info);
	        // ������ �������������� ������
	        adb.setPositiveButton(R.string.continueMeasurement, myClickListener);
	        // ������ �������������� ������
	        adb.setNegativeButton(R.string.saveMeasurement, myClickListener);
	        // ������ ������������ ������
	        adb.setNeutralButton(R.string.quitMeasurement, myClickListener);
	        // ������� ������
	        return adb.create();
		}
	    return super.onCreateDialog(id);
	}*/
	
	/*OnClickListener myClickListener = new OnClickListener() {
	    public void onClick(DialogInterface dialog, int which) {
	      switch (which) {
	      // ������������� ������
	      case Dialog.BUTTON_POSITIVE:
	        //saveData();
	        //finish();
	        break;
	      // ���������� ������
	      case Dialog.BUTTON_NEGATIVE:
	        //finish();
	        break;
	      // ����������� ������  
	      case Dialog.BUTTON_NEUTRAL:
	        break;
	      }
	    }
	  };*/
}
