package com.example.intencitycounter.simpleClasses;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"mode", "modeRate", "count"})
@Root(name="measurementList")
public class MeasurementList {
   
   @Element(name="mode")
   private String mode;
   
   @Element(name="modeRate")
   private double modeRate;
   
   @Element(name="count")
   private int count;
   
   public MeasurementList() {
      super();
   }  

   public MeasurementList(String mode, double modeRate, int count) {
      this.mode = mode;
      this.modeRate = modeRate;
      this.count = count;
   }

   public String getMode() {
      return mode;
   }

   public double getModeRate() {
      return modeRate;
   }
   
   public int getCount() {
	   return count;
   }
}

