package com.example.intencitycounter.simpleClasses;

import java.util.Collection;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"Latitude", "Longitude", "streetList"})
@Root(name="intersection")
public class Intersection {

	@Element(name="Latitude")
	private double latitude;
	
	@Element(name="Longitude")
	private double longitude;
	
	@ElementList(inline=true, entry="streetList")
	private Collection<String> list;
	
	public Intersection() {
		super();
	}
	
	public Intersection(double latitude, double longitude, Collection<String> list) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.list = list;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public Collection<String> getList() {
		return list;
	}
}