package com.example.intencitycounter.simpleClasses;

import java.util.Collection;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"direction", "measurementList"})
@Root(name="destination")
public class Destination {
	
	@Element
	private DirectionSC direction;
	
	@ElementList(inline=true, entry="measurementList")
	private Collection<MeasurementList> list;
	
	public void setList(Collection<MeasurementList> list) {
		this.list = list;
	}
	
	public Collection<MeasurementList> getList() {
		return list;
	}

	public Destination(DirectionSC direction, Collection<MeasurementList> list) {
		this.direction = direction;
		this.list = list;
	}
	
	public Destination() {
		super();
	}
	
	public DirectionSC getDirection() {
		return direction;
	}
}
