package com.example.intencitycounter.simpleClasses;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"ID", "street"})
@Root(name="direction")
public class DirectionSC {
	
	@Element(name="ID")
	private int id;

	@Element(name="street")
	private String street;
	
	public DirectionSC() {
		super();
	}
	
	public DirectionSC(int id, String street) {
		this.id = id;
		this.street = street;
	}
	
	public int getId() {
		return id;
	}
	
	public String getStreet() {
		return street;
	}
}
