package com.example.intencitycounter.simpleClasses;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"region", "country"})
@Root(name="location")
public class LocationSC {
	
	public LocationSC(String region, String country) {
		this.region = region;
		this.country = country;
	}

	public LocationSC() {
		super();
	}

	@Element(name="region")
	private String region;
	
	public String getRegion() {
		return region;
	}

	public String getCountry() {
		return country;
	}

	@Element(name="country")
	private String country;
}
