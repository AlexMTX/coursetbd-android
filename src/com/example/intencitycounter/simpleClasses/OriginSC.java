package com.example.intencitycounter.simpleClasses;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"ID", "street"})
@Root(name="origin")
public class OriginSC {

	public OriginSC(int id, String street) {
		this.id = id;
		this.street = street;
	}

	public OriginSC() {
		super();
	}

	@Element(name="ID")
	private int id;
	
	@Element(name="street")
	private String street;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
	
}
