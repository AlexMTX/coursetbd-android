package com.example.intencitycounter.simpleClasses;

import java.util.Collection;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements={"startTime", "period", "intersection", "location", "origin", "destination"})
@Root(name="Form")
public class Form {

	@Attribute(name="observer")
	private String observer;
	
	@Attribute(name="date")
	private String date;
	
	@Element(name="startTime")
	private String time;
	
	@Element(name="period")
	private int period;
	
	@Element(name="intersection")
	private Intersection intersection;
	
	@Element(name="location")
	private LocationSC location;
	
	@Element(name="origin")
	private OriginSC origin;

	@ElementList(inline=true, entry="destination")
	private Collection<Destination> destinationList;
	
	public Collection<Destination> getDestinationList() {
		return destinationList;
	}
	
	public OriginSC getOrigin() {
		return origin;
	}

	public Form() {
		super();
	}
	
	public Form(String date, String observer, String time, int period, 
				Intersection intersection, LocationSC location, OriginSC origin, 
				Collection<Destination> destinationList) {
		this.intersection = intersection;
		this.time = time;
		this.period = period;
		this.date = date;
		this.observer = observer;
		this.location = location;
		this.origin = origin;
		this.destinationList = destinationList;
	}
	
	public LocationSC getLocation() {
		return location;
	}
	
	public String getStartTime() {
		return time;
	}
	
	public int getPeriod() {
		return period;
	}
	
	public Intersection getIntersection() {
		return intersection;
	}
	
	public String getObserver() {
		return observer;
	}
	
	public String getDate() {
		return date;
	}
}
