package com.example.intencitycounter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	
	/* Choosing New Measurement */
	public void createNewMeasurement (View view) {
    	Intent intent = new Intent(this, SettingActivity.class);
    	startActivity(intent);
	}
	
	/* Choosing Watch content */
	public void watch(View view) {
		Intent intent = new Intent(this, WatchActivity.class);
		startActivity(intent);
	}
	
	/* Choosing Synhro */
	public void synhronize(View view) {
		//postData();
		Toast.makeText(MainActivity.this, "�� ����� �����", Toast.LENGTH_SHORT).show();
	}
	
	/* Choosing Developers */
	public void showDevelopers(View view) {
		Toast.makeText(MainActivity.this, "Copyright � by Silicium�, 2014", Toast.LENGTH_SHORT).show();
	}
	
	/* Choosing Quit */
	public void quitApp(View view) {
		Toast.makeText(MainActivity.this, "������ ���������", Toast.LENGTH_SHORT).show();
		finish();
	}
	
	@Override
	  protected void onDestroy() {
	    super.onDestroy();
	    System.out.println("MainActivity: onDestroy()  ~bye~");
	  }
	
	/* Not working. =[ */
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String[] arr = {"1", "2", "3"};
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(R.string.choose_files)
	           .setItems(arr, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	               // The 'which' argument contains the index position
	               // of the selected item
	           }
	    });
	    return builder.create();
	}
	
	/*public void postData() {
	    // �������� HttpClient � PostHandler
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("http://www.moisaitik.ru/postdemo.php");

	    try {
	        // ������� ������ (���� - "�������� - ��������")
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("login", "andro"));
	        nameValuePairs.add(new BasicNameValuePair("text", "������!"));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // �������� ������
	        HttpResponse response = httpclient.execute(httppost);
	        
	        String responseString = new String();
	        HttpEntity responseEntity = response.getEntity();
	        if(responseEntity!=null) {
	        	responseString = EntityUtils.toString(responseEntity);
	        }
	        Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_LONG).show();

	    } catch (ClientProtocolException e) {
	        // ������ :(
	    } catch (IOException e) {
	        // ������ :(
	    }*/
	

}
